import { readGameInfo, readStepInfo, writeStep } from "./io";
import { step } from "./logic";
import { makeState } from "./state";

const state = makeState()
const gameInfo = readGameInfo()
let cycle = 0
while (true) {
    const stepInfo = readStepInfo()
    writeStep(step(gameInfo, stepInfo, 0, cycle, state))
    writeStep(step(gameInfo, stepInfo, 1, cycle, state))
    ++cycle;
}