export function equals(a, b, epsilon) {
    epsilon = epsilon || 0.001
    return Math.abs(a - b) <= epsilon
}
