export function index(row, column, size = 4) {
    return row * size + column
}

export function degreesToRadians(degrees) {
    return degrees * (Math.PI / 180)
}

export function radiansToDegrees(radians) {
    return radians / Math.PI * 180
}

export function clamp(x, min, max) {
    if (x < min) {
        return min
    }
    if (x > max) {
        return max
    }
    return x
}

export function simplifyAngle(angle) {
    // reduce the angle  
    angle = angle % 360
    // force it to be the positive remainder, so that 0 <= angle < 360  
    angle = (angle + 360) % 360
    // force into the minimum absolute value residue class, so that -180 < angle <= 180  
    if (angle > 180)
        angle -= 360
    return angle
}
