import { equals } from "./equals"

export function vector(x, y, z, w) {
    const vector = new Float32Array(4)
    vector[0] = x || 0
    vector[1] = y || 0
    vector[2] = z || 0
    vector[3] = w || 0
    return vector
}

export function vectorAdd(a, b, out) {
    for (let i = 0; i < 4; i++) {
        out[i] = a[i] + b[i]
    }
    return out
}

export function vectorSubtract(a, b, out) {
    for (let i = 0; i < 4; i++) {
        out[i] = a[i] - b[i]
    }
    return out
}

export function vectorMultiply(a, b, out) {
    for (let i = 0; i < 4; i++) {
        out[i] = a[i] * b[i]
    }
    return out
}

export function vectorDivide(a, b, out) {
    for (let i = 0; i < 4; i++) {
        out[i] = a[i] / b[i]
    }
    return out
}

export function vectorInverse(v, out) {
    for (let i = 0; i < 4; i++) {
        out[i] = -v[i]
    }
    return out
}

export function vectorLengthSquared(v) {
    let out = 0
    for (let i = 0; i < 4; i++) {
        out += v[i] ** 2
    }
    return out
}

export function vectorLength(v) {
    return Math.sqrt(vectorLengthSquared(v))
}

export function vectorEquals(a, b, epsilon) {
    for (let i = 0; i < 4; i++) {
        if (!equals(a[i], b[i], epsilon)) {
            return false
        }
    }
    return true
}

export function vectorNormalize(v, out) {
    const len = vectorLength(v) || 1
    for (let i = 0; i < 4; i++) {
        out[i] = v[i] / len
    }
    return out
}

export function vectorCross(a, b, out) {
    out[0] = a[1] * b[2] - a[2] * b[1]
    out[1] = a[2] * b[0] - a[0] * b[2]
    out[2] = a[0] * b[1] - a[1] * b[0]
    out[3] = 0
    return out
}

export function vectorDot(a, b) {
    let out = 0
    for (let i = 0; i < 4; i++) {
        out += a[i] * b[i]
    }
    return out
}
