import { vector, vectorSubtract, vectorLength, vectorDot, vectorNormalize, vectorLengthSquared, vectorAdd } from "./math/vector"
import { clamp, radiansToDegrees, simplifyAngle } from "./math/mappings"
import { StateType, POD_RADIUS, CHECKPOINT_RADIUS, updateCheckpointsInfo, getWorstAlliedPodId, getBestEnemyPodId } from "./state"

export function step(gameInfo, stepInfo, podId, cycle, state) {
    updateCheckpointsInfo(state, stepInfo)

    const pod = stepInfo.allies[podId]
    let checkpoint = gameInfo.checkpoints[pod.nextCheckpointId]

    const preTurnSpeedThreshold = 200
    const preTurnThreshold = 2
    if (vectorLengthSquared(pod.velocity) > preTurnSpeedThreshold ** 2 &&
        vectorLength(vectorSubtract(checkpoint, pod.position, vector())) - CHECKPOINT_RADIUS < POD_RADIUS * preTurnThreshold) {
        console.error("preselect")
        checkpoint = gameInfo.checkpoints[(pod.nextCheckpointId + 1) % gameInfo.checkpoints.length]
    }

    const isBully = podId == getWorstAlliedPodId(state)
    if (isBully) {
        const enemyId = getBestEnemyPodId(state)
        const enemy = stepInfo.enemies[enemyId]
        const enemyTarget = gameInfo.checkpoints[enemy.nextCheckpointId]
        const enemyDesiredMoveVector = vectorSubtract(enemyTarget, enemy.position, vector())
        const scaledEnemyDesiredMoveVector = enemyDesiredMoveVector.map(x => x * 0.5)
        checkpoint = vectorAdd(enemy.position, scaledEnemyDesiredMoveVector, vector())
    }

    const targetDirection = vectorSubtract(checkpoint, pod.position, vector())
    const targetDist = vectorLength(targetDirection)
    const targetAngle = radiansToDegrees(Math.atan2(targetDirection[1], targetDirection[0]))

    const angleToCheckpoint = simplifyAngle(Math.round(targetAngle - pod.angle))

    const turnCompensationFactor = 2
    const targetPosition = vectorSubtract(checkpoint, pod.velocity.map(x => x * turnCompensationFactor), vector())

    const offset = 0.1
    const heading = ((1 - Math.abs(angleToCheckpoint / 180)) + offset) ** 2
    const deaccelThreshold = POD_RADIUS * 6
    const accelFactor = isBully ? 1
        : Math.abs(angleToCheckpoint) < 10 && targetDist < deaccelThreshold ? targetDist / deaccelThreshold : 1
    const thrust = clamp(heading * accelFactor * 100, 0, 100)

    const boostCondition = cycle > 60
        && Math.random() > 0.5 || cycle > 120
    let boost = targetDist > 7000 
        && Math.abs(angleToCheckpoint) < 5
        && boostCondition

    boost = isBully && cycle == 0 ? true : boost

    const shieldDistance = 400 * 3
    const shieldCondition = cycle > 10 && vectorLength(pod.velocity) > 400
    const shield = shieldCondition && stepInfo.enemies.some(enemy => {
        const directionsOpposite = vectorDot(
            vectorNormalize(pod.velocity, vector()),
            vectorNormalize(enemy.velocity, vector())
        ) < 0
        const closestEnemyDist = vectorLength(vectorSubtract(pod.position, enemy.position, vector()))
        return directionsOpposite && closestEnemyDist < shieldDistance
    })

    return {
        position: targetPosition.map(Math.round),
        thrust: shield ? "SHIELD" : boost ? "BOOST" : Math.ceil(thrust),
        message: isBully ? "meh" : ''
    }
}
