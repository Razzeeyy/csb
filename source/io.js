import { vector } from "./math/vector"

function readCheckpoint() {
	const checkpoint = readline().split(' ').map(x => +x)
	return Object.freeze(checkpoint)
}

export function readGameInfo() {
	const laps = +readline()
	const checkpoints = new Array(+readline()).fill([])
		.map(readCheckpoint)
		.map(([x, y]) => vector(x, y))
	return Object.freeze({
		laps,
		checkpoints: Object.freeze(checkpoints)
	})
}

function readPod() {
	const [x, y, vx, vy, angle, nextCheckpointId] = readline().split(' ').map(x => +x)
	return Object.freeze({
		position: vector(x, y),
		velocity: vector(vx, vy),
		angle,
		nextCheckpointId
	})
}

export function readStepInfo() {
	return Object.freeze({
		allies: [readPod(), readPod()],
		enemies: [readPod(), readPod()]
	})
}

export function writeStep({ position, thrust, message }) {
	if (message) {
		console.log(position[0], position[1], thrust, message)
	} else {
		console.log(position[0], position[1], thrust)
	}
}
