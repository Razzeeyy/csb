export const CHECKPOINT_RADIUS = 600
export const POD_RADIUS = 400

function makeCheckpointsInfo() {
    return {
        lastNextCheckpoint: 1,
        checkpointsPassed: 0
    }
}

export function makeState() {
    return {
        currentCheckpoint: [0, 0],
        alliesCheckpointsInfo: [makeCheckpointsInfo(), makeCheckpointsInfo()],
        enemiesCheckpointsInfo: [makeCheckpointsInfo(), makeCheckpointsInfo()],
        lastBestEnemyId: 0
    }
}

export function updateCheckpointsInfo(state, stepInfo) {
    const { enemiesCheckpointsInfo, alliesCheckpointsInfo } = state
    const { allies, enemies } = stepInfo
    allies.forEach((podInfo, podId) => {
        updatePodCheckpointsInfo(podInfo, alliesCheckpointsInfo[podId])
    })
    enemies.forEach((podInfo, podId) => {
        updatePodCheckpointsInfo(podInfo, enemiesCheckpointsInfo[podId])
    })
}

function updatePodCheckpointsInfo(podInfo, podCheckpointsInfo) {
    if (podCheckpointsInfo.lastNextCheckpoint != podInfo.nextCheckpointId) {
        podCheckpointsInfo.checkpointsPassed += 1
        podCheckpointsInfo.lastNextCheckpoint = podInfo.nextCheckpointId
    }
}

export function getWorstAlliedPodId(state) {
    const [pod0, pod1] = state.alliesCheckpointsInfo
    return pod0.checkpointsPassed < pod1.checkpointsPassed ? 0 : 1
}

export function getBestEnemyPodId(state) {
    const [enemy0, enemy1] = state.enemiesCheckpointsInfo
    const nextBestEnemyId = enemy0.checkpointsPassed > enemy1.checkpointsPassed ? 0 : 1
    if (nextBestEnemyId != state.lastBestEnemyId
        && enemy0.checkpointsPassed != enemy1.checkpointsPassed) {
        state.lastBestEnemyId = nextBestEnemyId
    }
    return state.lastBestEnemyId
    
}
