const path = require('path');

module.exports = {
  entry: './source/main.js',
  mode: 'production',
  optimization: {
    minimize: false
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
  },
};
